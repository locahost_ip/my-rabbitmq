package cn.blogs.show.everythin.work.queues.consumer;

import com.rabbitmq.client.*;
import utils.RabbitMqUtils;

import java.io.IOException;

public class WorkQueuesConsumerSecond {
    public static void main(String[] args) throws Exception {
        Connection connection = null;
        Channel channel = null;

        try {
            // 获取连接
            connection = RabbitMqUtils.getConnection();
            if (connection != null) {
                // 获取通道
                channel = connection.createChannel();
                channel.queueDeclare("work_queues_test", true, false, false, null);
                // 消息
                DefaultConsumer consumer = new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                        System.out.println("Consumer-Second:" + new String(body));
                    }
                };
                channel.basicConsume("work_queues_test", true, consumer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
