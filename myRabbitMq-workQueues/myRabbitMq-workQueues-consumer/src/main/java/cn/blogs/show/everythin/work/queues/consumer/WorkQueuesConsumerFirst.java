package cn.blogs.show.everythin.work.queues.consumer;

import com.rabbitmq.client.*;
import utils.RabbitMqUtils;

import java.io.IOException;

public class WorkQueuesConsumerFirst {
    // 和简单模式一样，只是一个生产者，多个消费者,交替消费消息

    public static void main(String[] args) {
        Connection connection = null;
        Channel channel = null;

        try {
            // 获取连接
            connection = RabbitMqUtils.getConnection();
            if (connection != null) {
                // 获取通道
                channel = connection.createChannel();
                channel.queueDeclare("work_queues_test", true, false, false, null);
                // 消费消息
                DefaultConsumer consumer = new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                        System.out.println("Consumer-First:" + new String(body));
                    }
                };
                channel.basicConsume("work_queues_test", true, consumer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
