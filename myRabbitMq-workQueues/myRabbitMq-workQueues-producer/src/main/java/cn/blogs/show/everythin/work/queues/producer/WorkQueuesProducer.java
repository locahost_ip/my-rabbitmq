package cn.blogs.show.everythin.work.queues.producer;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import pojo.Student;
import utils.RabbitMqUtils;

import java.io.IOException;

public class WorkQueuesProducer {

    // 工作队列(又名任务队列)，当消息处理比较耗时的时候，可能生产消息的速度会远远大于消息的消费速度。
    // 消息堆积越来越多无法得到及时处理。此时可以使用work模型：
    // 让多个消费者绑定一个队列，共同消费队列中的消息。队列中的消息一旦消费就会消失，因此任务不会被重复执行
    public static void main(String[] args) {
        Connection connection = null;
        Channel channel = null;
        try {
            // 创建链接
            connection = RabbitMqUtils.getConnection();
            if (connection != null) {
                // 获取通道对象
                channel = connection.createChannel();
                // 声明队列
                channel.queueDeclare("work_queues_test", true, false, false, null);
                // 生产消息
                for (int i = 1; i <= 20; i++) {
                    Student s = new Student("echo", i);
                    String jsonString = JSON.toJSONString(s);
                    System.out.println(jsonString);
                    channel.basicPublish("", "work_queues_test", null, jsonString.getBytes());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            RabbitMqUtils.close(channel, connection);
        }
    }
}
