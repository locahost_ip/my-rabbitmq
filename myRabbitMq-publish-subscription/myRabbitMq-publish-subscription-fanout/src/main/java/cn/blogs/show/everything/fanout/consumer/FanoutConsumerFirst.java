package cn.blogs.show.everything.fanout.consumer;

import com.rabbitmq.client.*;
import utils.RabbitMqUtils;

import java.io.IOException;

public class FanoutConsumerFirst {
    public static void main(String[] args) {
        Connection connection = null;
        Channel channel = null;

        try {
            connection = RabbitMqUtils.getConnection();
            if (connection != null) {
                channel = connection.createChannel();
                String exchangeName = "pub_sub_fanout_exchange";
                channel.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT);
                String q1 = "pub_sub_fanout_first_queues";
                channel.queueDeclare(q1, true, false, false, null);
                channel.queueBind(q1, exchangeName, "");
                // 如果 交换机已经声明,以下方法可以获取绑定的队列
//                String queue = channel.queueDeclare().getQueue();
//                channel.queueBind(q1, exchangeName, "");
                channel.basicConsume(q1, true, new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                        System.out.println("fanout_first_consumer :" + new String(body));
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
