package cn.blogs.show.everything.fanout.producer;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import utils.RabbitMqUtils;

// 广播
// 将接受到的所有消息广播给所有已知的队列
public class FanoutProducer {
    public static void main(String[] args) {
        Connection connection = null;
        Channel channel = null;
        try {
            connection = RabbitMqUtils.getConnection();
            if (connection != null) {
                channel = connection.createChannel();
                // 声明交换机
//                exchangeDeclare(
//                String exchange,  交换机名称
//                BuiltinExchangeType type, 交换机类型

//                DIRECT("direct"), 定向
//                FANOUT("fanout"), 广播
//                TOPIC("topic"), 通配符
//                HEADERS("headers"); 参数匹配

//                boolean durable,
//                boolean autoDelete,
//                boolean internal,
//                Map<String, Object> arguments)
                String exchangeName = "pub_sub_fanout_exchange";
                channel.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT, false, false, false, null);
                // 声明队列
                String q1 = "pub_sub_fanout_first_queues";
                String q2 = "pub_sub_fanout_second_queues";
                channel.queueDeclare(q1, true, false, false, null);
                channel.queueDeclare(q2, true, false, false, null);
                // 交换机与队列绑定
                channel.queueBind(q1, exchangeName, "");
                channel.queueBind(q2, exchangeName, "");
                // 发送消息
                String body = "fanout message !!!!";
                channel.basicPublish(exchangeName, "", null, body.getBytes());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RabbitMqUtils.close(channel, connection);
        }
    }
}
