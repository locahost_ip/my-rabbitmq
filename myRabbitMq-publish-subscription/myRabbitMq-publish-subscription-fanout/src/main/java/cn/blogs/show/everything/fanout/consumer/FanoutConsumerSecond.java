package cn.blogs.show.everything.fanout.consumer;

import com.rabbitmq.client.*;
import utils.RabbitMqUtils;

import java.io.IOException;

public class FanoutConsumerSecond {
    public static void main(String[] args) {
        Connection connection = null;
        Channel channel = null;

        try {
            connection = RabbitMqUtils.getConnection();
            if (connection != null) {
                channel = connection.createChannel();
                String exchangeName = "pub_sub_fanout_exchange";
                channel.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT);
                String q2 = "pub_sub_fanout_second_queues";
                channel.queueDeclare(q2, true, false, false, null);
                channel.queueBind(q2, exchangeName, "");
                channel.basicConsume(q2, true, new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                        System.out.println("fanout_second_consumer : " + new String(body));
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
