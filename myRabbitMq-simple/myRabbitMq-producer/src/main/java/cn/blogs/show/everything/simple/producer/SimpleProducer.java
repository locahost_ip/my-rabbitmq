package cn.blogs.show.everything.simple.producer;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import pojo.Student;
import utils.RabbitMqUtils;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2021/12/12
 * Time: 9:21
 *
 * @author Administrator
 * <p>
 * <p>
 * 简单模式
 */

public class SimpleProducer {
    public static void main(String[] args) {

        Connection connection = null;
        Channel channel = null;
        try {
            // 获取连接
            connection = RabbitMqUtils.getConnection();
            if (connection != null) {
                // 创建 channel
                channel = connection.createChannel();
                // 通道绑定对应消息队列
        /*
        queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete,Map<String, Object> arguments)
        参数：
        1. queue 队列名称(不存在自动创建)
        2. durable 用来定义队列特性是否需要持久化(为true该队列将在服务器重启后保留下来，持久化到硬盘中)
        3. exclusive 是否独占队列(为true仅限此连接)
        4. autoDelete  是否在消费完成后自动删除队列
        5. arguments 队列的其他属性（构造参数）
         */
                // 如果没有一个名字叫 testMq 的队列，则会创建该队列,如果有则不会创建

                channel.queueDeclare("testMq", true, false, false, null);


                // 发布消息
                // 简单模式不需要 交换机 exchange
        /*
       basicPublish(String exchange, String routingKey, AMQP.BasicProperties props, byte[] body)
       参数：
       1. exchange 交换机名称,简单模式下交换机使用默认的为 "",要将消息发布到的交换机
       2. routingKey 路由名称,简单模式下 路由只要和  上面 queue 队列名称相同,就可以绑定,路由键，指定队列
       3. props 消息的其他属性
       4. body 消息具体内容

         */
//        String body = "hello rabbitMq-server";
                Student s = new Student("echo", 22);
                String jsonString = JSON.toJSONString(s);
                System.out.println(jsonString);

                channel.basicPublish("", "testMq", null, jsonString.getBytes());
                System.out.println("生产者发布消息成功");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            RabbitMqUtils.close(channel, connection);
        }
    }
}
