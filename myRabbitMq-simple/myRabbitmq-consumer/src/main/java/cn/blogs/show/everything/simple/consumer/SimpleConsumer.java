package cn.blogs.show.everything.simple.consumer;

import com.rabbitmq.client.*;
import utils.RabbitMqUtils;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Created By CQ
 * Date: 2021/12/12
 * Time: 10:14
 * <p>
 * 简单模式
 */
public class SimpleConsumer {
    public static void main(String[] args) {


        Connection connection = null;
        Channel channel = null;
        try {
            // 获取连接
            connection = RabbitMqUtils.getConnection();
            if (connection != null) {
                // 获取通道
                channel = connection.createChannel();
                // 绑定对应的消息队列
        /*
        queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete,Map<String, Object> arguments)
        参数：
        1. queue 队列名称(不存在自动创建)
        2. durable 用来定义队列特性是否需要持久化(为true该队列将在服务器重启后保留下来)
        3. exclusive 是否独占队列(为true仅限此连接)
        4. autoDelete  是否自动删除,当没有connection 链接时,自动删除队列,是否在消费完成不再使用后自动删除队列
        5. arguments 队列的其他属性（构造参数）
         */
                // 如果没有一个名字叫 testMq 的队列，则会创建该队列,如果有则不会创建
                channel.queueDeclare("testMq", true, false, false, null);
                //消费消息
                DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
                    //获取消息并且处理。此方法类似于事件监听，有消息时会被自动调用
                    /*
                    回调方法,当收到消息，会自动执行该方法
                    1. consumerTag 标识
                    2. envelope 获取一些信息,交换机,路由key
                   3. properties 配置信息
                    4. body 数据
                   */
                    @Override
                    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
//                        System.out.println("consumerTa :" + consumerTag);
//                        System.out.println("Exchange:" + envelope.getExchange());
//                        System.out.println("RoutingKey:" + envelope.getRoutingKey());
//                        System.out.println("properties:" + properties);
                        // 消息本体
                        System.out.println("body:" + new String(body));

                    }

                };

        /*
         queue 队列
         autoAck 自动应答
         callback 回调对象
         */

                channel.basicConsume("testMq", true, defaultConsumer);
            }
            System.out.println("消费者消费消息完成......");
        } catch (IOException e) {
            e.printStackTrace();
        }
//        finally {
        // 注意：希望消费者方能一直监听队列中是否有新的消息，所以不关闭通道和连接。
        // 程序不会停止，一旦有新的消息进入队列，会立即消费
//        RabbitMqUtils.close(channel, connection);
//        }
    }
}
